autoload -U colors && colors
PS1="%B%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[white]%}%{$FG[008]%} λ%{$reset_color%}%b "

# Load aliases and shortcuts if existent.

# file shortcuts
alias h="cd ~/ && ls -a" \
d="cd ~/Documents && ls -a" \
D="cd ~/Downloads && ls -a" \
m="cd ~/Music && ls -a" \
pp="cd ~/Pictures && ls -a" \
p="cd ~/Projects && ls" \
vv="cd ~/Videos && ls -a" \
cf="cd ~/.config && ls -a" \
sc="cd ~/.local/bin && ls -a" \
ds="cd ~/.local/bin/drake && ls -a" \
src="cd ~/.local/src && ls -a" \
V="cd ~/.local/src/ymir/void-packages && ls" \
Vp="cd ~/.local/src/void-packages && ls" \
mn="cd /mnt && ls -a" \
bf="$EDITOR ~/.config/files" \
bd="$EDITOR ~/.config/directories" \
bw="$EDITOR ~/.config/bookmarks" \
cfa="$EDITOR ~/.config/aliasrc" \
cfz="$EDITOR $ZDOTDIR/.zshrc" \
cfv="$EDITOR ~/.config/nvim/init.vim" \
cfm="$EDITOR ~/.config/mutt/muttrc" \
cfd="$EDITOR ~/.Xdefaults" \
cfu="$EDITOR ~/.config/newsboat/urls" \
cfn="$EDITOR ~/.config/newsboat/config" \
cfmb="$EDITOR ~/.config/ncmpcpp/bindings" \
cfmc="$EDITOR ~/.config/ncmpcpp/config" \
cfk="$EDITOR ~/.config/sxhkd/sxhkdrc" \
cfi="$EDITOR ~/.config/i3/config" \
cfb="$EDITOR ~/.config/i3blocks/config" \

# Use neovim for vim if present.
command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d"

# Verbosity and settings that you pretty much just always are going to want.
alias \
	bat="cat /sys/class/power_supply/BAT?/capacity" \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -v" \
	mkd="mkdir -pv" \
	yt="youtube-dl --add-metadata -i" \
	yta="yt -x -f bestaudio/best" \
	ffmpeg="ffmpeg -hide_banner"

# Colorize commands when possible.
alias \
	ls="ls -hN --color=auto --group-directories-first" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi"

# These common commands are just too long! Abbreviate them.
alias \
	ka="killall" \
	g="git" \
	gc="git commit" \
	gp="git push" \
	gpl="git pull" \
	gplu="git pull upstream master" \
	gcl="git clone" \
	trem="transmission-remote" \
	YT="straw-viewer" \
	sdn="sudo shutdown -h now" \
	f="$FILE" \
	e="emacsclient" \
	v="$EDITOR" \
	xr="sudo xbps-remove -R" \
	xu="sudo xbps-install -Su" \
	xq="xbps-query" \
	xp="cd ~/.local/src/ymir/void-packages && git pull --no-edit upstream master && ./xbps-src bootstrap-update && git push" \
	xpp="cd ~/.local/src/void-packages && git pull --no-edit upstream master && ./xbps-src bootstrap-update && git push" \
	xds="xdeb -Sde" \
	xdi="sudo xbps-install -R ~/.local/src/xdeb/binpkgs" \
	vimg="cd ~/.local/src/void-mklive && sudo ./mklive.sh -r http://198.255.68.182 -T 'Void GNU/Linux-libre'" \
	chrup="sudo xbps-install -S --repository http://192.168.1.102 chromium"
	# p="sudo pacman" \
	# xi="sudo xbps-install" \

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

# Include hidden files in autocomplete:
_comp_options+=(globdots)

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

export KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'

  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init

# Use beam shape cursor on startup.
echo -ne '\e[5 q'
# Use beam shape cursor for each new prompt.
preexec() { echo -ne '\e[5 q' ;}

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir"
            fi
        fi
    fi
}

bindkey -s '^o' 'lfcd\n'  # zsh

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
